---
marp: true
theme: uncover
_class: invert
---

# Linux Install Day

###### oktobar 2021
###### Departman za matematiku i infromatiku
###### Univerzitet u Novom Sadu

![bg right : 60%](linux.svg)

---

# Šta je Linux?

---

### Šta je Linux?

* Operativni sistem
* Podeljen u distribucije (Fedora, Ubuntu, PopOS, Arch Linux)
* Android? (mi ga nećemo računati)
* Distribucije su u srodstvu

---

![bg contain : 40%](debian.svg)
![bg contain : 80%](redhat.svg)
![bg contain : 80%](slackware.svg)

---

[Stablo](https://en.wikipedia.org/wiki/List_of_Linux_distributions)

---

### Neki drugi termini

* GNU/Linux
* Linuxoliki (eng. Linux like)
* Bazirani na Linux-u (eng. Linux based)

---

# Zašto Linux?

---

### Zašto Linux?

* Serveri
* Embedded sistemi (video nadzor, automobili, ruteri...)
* Superračunari
* Veliki broj poslova zahteva bar osnovno znanje Linux-a
* Jedan od najpogodnijih operativnih sistema za programiranje

---

### Prednosti u programiranju

* Otvorenost koda i sistema
* Prilagodljivost
* Dobra dokumentacija

---

askubuntu.com
ask.fedoraproject.org
wiki.archlinux.org

---

### Prednosti u uobičajenom korišćenju

* Update-uje se kada mu vi kažete
* Laganiji sistem
* Prilagodljiv

---

# Mane

---

### Mane

* Ne može da porkeće Windows softver*
	* Dobra podrška za igrice (ProtonDB, Lutris)
* Nekompatibilan hardver*
	* U jako specijalnim slučajevima koji su često rešivi

---

### Česti alati

* ~~Microsoft Office~~ LibreOffice
* ~~Adobe Photoshop~~ GIMP, Photopea
* ~~Adobe Illustrator~~ Inkscape
* ~~Adobe Lightroom~~ Darktable
* ~~Adobe Reader~~ Default opcija
	* Okular

---

# Package manager

---

### Package manager

* Zadužen za instaliranje, brisanje i ažuriranje programa
* Provera zavisnih programa i kompatibilnosti

```sh
# Ubuntu
$ sudo apt install <program>
$ sudo apt upgrade
$ sudo apt purge <program>
# Fedora
$ sudo dnf install <program>
$ sudo dnf update
$ sudo dnf remove <program>
```

---

### Repozitorijumi paketa

* Preuzimanje iz repozitorijuma paketa
* Softver je donekle prošao kontrolu kvaliteta
* Flatpak (Fedora), Snap (Ubuntu), AppImage

---

# Grafički alati za održavanje paketa

---

![bg contain](gnome-software-1.png)

---

![bg contain](gnome-software-2.png)

---

![bg contain : 80%](dnfdragora.png)

---

# Instaliranje!
